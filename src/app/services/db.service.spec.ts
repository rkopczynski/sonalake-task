import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AppConfigModule } from '../app.config.module';
import { StarWarsDbService } from './db.service';
import { Character } from '../models';
import { of } from 'rxjs';

describe('StarWarsDbService', () => {
  let http: HttpClient;
  let service: StarWarsDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppConfigModule,

        HttpClientTestingModule,
      ],
      providers: [StarWarsDbService],
    });

    http = TestBed.get(HttpClient);
    service = TestBed.get(StarWarsDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`should call all simple endpoints`, () => {
    let spy = spyOn(http, 'get');

    service.getCharacter('1');
    service.getSpecies();
    service.getSearchedCharacters('test');
    expect(spy.calls.count()).toBe(3);

    spy = spyOn(http, 'put');
    service.putCharacter(<Character>{});
    expect(spy.calls.count()).toBe(1);

    spy = spyOn(http, 'delete');
    service.removeCharacter(1);
    expect(spy.calls.count()).toBe(1);

    spy = spyOn(http, 'post');
    service.postCharacter(<Character>{});
    expect(spy.calls.count()).toBe(1);
  });

  it(`should call 'getInitCharacters' & 'getPagedCharacters'`, () => {
    // tslint:disable-next-line:max-line-length
    const linkHeader = `<http://localhost:3000/characters?_page=1&_limit=10>; rel="first", <http://localhost:3000/characters?_page=2&_limit=10>; rel="next", <http://localhost:3000/characters?_page=2&_limit=10>; rel="last"`;
    spyOn(http, 'get').and.returnValue(of({
      body: [],
      headers: {
        get: () => linkHeader
      },
      url: 'fake-url'
    }));

    service.getInitCharacters().subscribe(res => {
      expect(res.characters.length).toBe(0);
      expect(res.current).toBe('fake-url');
    });

    service.getPagedCharacters('test').subscribe(res => {
      expect(res.characters.length).toBe(0);
      expect(res.current).toBe('fake-url');
    });
  });
});
