import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { APP_CONFIG, AppConfig } from '../app.config.module';
import { Character } from '../models';
import { PagedCharacters } from '../models/paged-characters.model';
import { Observable } from 'rxjs';

type LinkType = 'first' | 'last' | 'next' | 'prev';

@Injectable({
  providedIn: 'root'
})
export class StarWarsDbService {
  // TODO RK: Tak można wyjąć dane do paginacji z headera 'Link'
  // Najpierw trzeba jednak wyjąć pojedyncze elementy (oddzielane przecinkiem)
  private readonly REGEX = `<(.*)>; rel="X"`;
  private readonly LINK_HEADER = 'Link';
  private readonly LIMIT = '5';

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
  ) { }

  getInitCharacters(): Observable<PagedCharacters> {
    return this.http.get<Character[]>(`${this.config.apiUrl}/characters`, {
      params: new HttpParams()
        .append('_page', '1')
        .append('_limit', this.LIMIT),
      observe: 'response'
    }).pipe(map(res => this._extractPagedData(res)));
  }

  getSearchedCharacters(text: string): Observable<Character[]> {
    return this.http.get<Character[]>(`${this.config.apiUrl}/characters`, {
      params: new HttpParams()
        .append('_limit', this.LIMIT)
        .append('q', text)
    });
  }

  getPagedCharacters(link: string): Observable<PagedCharacters> {
    return this.http
      .get<Character[]>(link, { observe: 'response' })
      .pipe(map(res => this._extractPagedData(res)));
  }

  getSpecies(): Observable<string[]> {
    return this.http.get<string[]>(`${this.config.apiUrl}/species`);
  }

  getCharacter(id: string): Observable<Character> {
    return this.http.get<Character>(`${this.config.apiUrl}/characters/${id}`);
  }

  removeCharacter(id: number) {
    return this.http.delete(`${this.config.apiUrl}/characters/${id}`);
  }

  putCharacter(requestData: Character) {
    return this.http.put<Character>(`${this.config.apiUrl}/characters/${requestData.id}`, requestData);
  }

  postCharacter(requestData: Character) {
    return this.http.post<Character>(`${this.config.apiUrl}/characters`, requestData);
  }

  private _findLink(type: LinkType, linkHeaderParts: string[]): string {
    const goodPart = linkHeaderParts.find(p => p.includes(`rel="${type}"`));
    if (!goodPart) { return null; }
    return new RegExp(this.REGEX.replace('X', type)).exec(goodPart)[1];
  }

  private _extractPagedData(response: HttpResponse<Character[]>) {
    const linkHeaderParts = response.headers.get(this.LINK_HEADER).split(',');

    return {
      characters: response.body,
      current: response.url,
      first: this._findLink('first', linkHeaderParts),
      prev: this._findLink('prev', linkHeaderParts),
      next: this._findLink('next', linkHeaderParts),
      last: this._findLink('last', linkHeaderParts),
    };
  }
}

