import { NgModule, InjectionToken } from '@angular/core';
import { environment } from '../environments/environment';

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export class AppConfig {
  apiUrl: string;
}

export const CONFIG: AppConfig = {
  apiUrl: environment.apiUrl
};

@NgModule({
  providers: [{
    provide: APP_CONFIG,
    useValue: CONFIG
  }]
})
export class AppConfigModule { }
