import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SwAddNewCharacterComponent } from './components/add-new-character/add-new-character.component';
import { ListViewComponent } from './components/list-view/list-view.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'character-list' },
  { path: 'character-list', component: ListViewComponent },
  { path: 'add-new-character', component: SwAddNewCharacterComponent },
  { path: 'edit-character/:id', component: SwAddNewCharacterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
