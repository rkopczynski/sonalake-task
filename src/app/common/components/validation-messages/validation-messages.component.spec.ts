import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationMessagesComponent } from './validation-messages.component';
import { ControlContainer, FormGroup, FormControl, Validators, FormGroupDirective, ValidationErrors } from '@angular/forms';

describe('ValidationMessagesComponent', () => {
  let component: ValidationMessagesComponent;
  let fixture: ComponentFixture<ValidationMessagesComponent>;
  let control: FormControl;

  beforeEach(async(() => {
    control = new FormControl('');
    const form = new FormGroup({
      control: control
    });
    const fgd = new FormGroupDirective([], []);
    fgd.form = form;

    TestBed.configureTestingModule({
      declarations: [ValidationMessagesComponent],
      providers: [
        { provide: ControlContainer, useValue: fgd }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationMessagesComponent);
    component = fixture.componentInstance;
    component.formControlName = 'control';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should call interface methods`, () => {
    const foo0 = () => { expect().nothing(); };
    const foo1 = () => { expect().nothing(); };

    const spy = spyOn(component, 'writeValue').and.callThrough();

    component.writeValue(null);
    expect(spy.calls.count()).toBe(1);

    component.registerOnChange(foo0);
    component.onChange();

    component.registerOnTouched(foo1);
    component.onTouched();
  });

  it(`should get correct errors`, () => {
    const customErrorMessage = `Control isn't valid. Cannot be 50.`;
    const customValidator = () => {
      return (c: FormControl): ValidationErrors => {
        if (c.value === 50) {
          return { customMessage: customErrorMessage };
        } else {
          return null;
        }
      };
    };

    control.setValidators([
      Validators.required,
      Validators.min(10),
      Validators.max(100),
      customValidator(),
    ]);

    control.setValue(null);
    expect(component.getErrors()[0]).toBe(
      'This field is required',
      'should be invalid (required error)'
    );

    control.setValue(1);
    expect(component.getErrors()[0]).toBe(
      `Value of this field shouldn't be smaller than ${10}`,
      'should be invalid (min error)'
    );

    control.setValue(101);
    expect(component.getErrors()[0]).toBe(
      `Value of this field shouldn't be bigger than ${100}`,
      'should be invalid (max error)'
    );

    control.setValue(50);
    expect(component.getErrors()[0])
      .toBe(customErrorMessage, 'should be invalid (custom error)');

    control.setValue(15);
    expect(component.getErrors().length).toBe(0, 'should be valid');
  });
});
