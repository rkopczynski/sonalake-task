import { Component, forwardRef, Host, Input, OnInit, Optional, SkipSelf } from '@angular/core';
import { ControlContainer, ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'sw-validation-messages',
  templateUrl: './validation-messages.component.html',
  styleUrls: ['./validation-messages.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ValidationMessagesComponent),
      multi: true
    }
  ],
})
export class ValidationMessagesComponent implements ControlValueAccessor, OnInit {
  @Input() formControlName: string;

  control: FormControl;

  onChange: () => void;
  onTouched: () => void;

  constructor(
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer
  ) { }

  ngOnInit() {
    if (this.controlContainer) {
      if (this.formControlName) {
        this.control = this.controlContainer.control.get(this.formControlName) as FormControl;
      } else {
        console.warn('Missing FormControlName directive from host element of the component');
      }
    } else {
      console.warn(`Can't find parent FormGroup directive`);
    }
  }

  writeValue(obj: any): void { }
  registerOnChange(fn: any): void { this.onChange = fn; }
  registerOnTouched(fn: any): void { this.onTouched = fn; }

  public getErrors(): string[] {
    if (this.control && this.control.errors) {
      const errors = Object.keys(this.control.errors).map(key => {
        const error = this.control.errors[key];

        switch (key) {
          case 'min': return `Value of this field shouldn't be smaller than ${error.min}`;
          case 'max': return `Value of this field shouldn't be bigger than ${error.max}`;
          case 'required': return `This field is required`;
          default: return `${error}`;

        }
      });

      return errors;
    } else {
      return [];
    }
  }
}
