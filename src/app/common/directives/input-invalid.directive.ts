import { Directive, DoCheck, ElementRef, Host, Input, OnInit, Optional, Renderer2, SkipSelf } from '@angular/core';
import { ControlContainer, FormControl } from '@angular/forms';

@Directive({
  selector: '[swInputInvalid]'
})
export class InputInvalidDirective implements OnInit, DoCheck {
  @Input() formControlName: string;

  private control: FormControl;

  constructor(
    private renderer: Renderer2,
    private elementRef: ElementRef,
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer,
  ) { }

  ngOnInit() {
    if (this.controlContainer) {
      if (this.formControlName) {
        this.control = this.controlContainer.control.get(this.formControlName) as FormControl;
      } else {
        throw new Error('Missing FormControlName directive from host element of the component');
      }
    } else {
      throw new Error(`Can't find parent FormGroup directive`);
    }
  }

  ngDoCheck() {
    if (this.control && this.control.touched && this.control.invalid) {
      this.renderer.addClass(this.elementRef.nativeElement, 'is-invalid');
    } else {
      this.renderer.removeClass(this.elementRef.nativeElement, 'is-invalid');
    }
  }
}
