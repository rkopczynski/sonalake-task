import { Component, Renderer2, ElementRef } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators, ReactiveFormsModule, FormGroupDirective } from '@angular/forms';

import { InputInvalidDirective } from './input-invalid.directive';

class MockRenderer {
  addClass() { }
  removeClass() { }
}

class MockElementRef { }

describe('InputInvalidDirective', () => {
  let directive: InputInvalidDirective;
  let renderer: Renderer2;
  let elementRef: ElementRef;
  let control: FormControl;

  beforeEach(() => {
    renderer = new MockRenderer() as any;
    elementRef = new MockElementRef() as any;
    control = new FormControl(null, Validators.required);
    const form = new FormGroup({
      control: control
    });
    const fgd = new FormGroupDirective([], []);
    fgd.form = form;

    directive = new InputInvalidDirective(renderer, elementRef, fgd);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
    directive.formControlName = 'control';
    directive.ngOnInit();
    expect().nothing();
  });

  it(`should throw when params are invalid`, () => {
    expect(() => new InputInvalidDirective(renderer, elementRef, null).ngOnInit()).toThrow();
    expect(() => directive.ngOnInit()).toThrow();
  });

  it(`should correctly set classes`, () => {
    const rSpy0 = spyOn(renderer, 'addClass');
    const rSpy1 = spyOn(renderer, 'removeClass');

    directive.formControlName = 'control';
    directive.ngOnInit();

    directive.ngDoCheck();
    expect(rSpy0.calls.count()).toBe(0);
    expect(rSpy1.calls.count()).toBe(1);

    control.markAsTouched();
    directive.ngDoCheck();
    expect(rSpy0.calls.count()).toBe(1);
    expect(rSpy1.calls.count()).toBe(1);

    control.setValue(1);
    directive.ngDoCheck();
    expect(rSpy0.calls.count()).toBe(1);
    expect(rSpy1.calls.count()).toBe(2);
  });
});
