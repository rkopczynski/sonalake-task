import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { of, zip, BehaviorSubject } from 'rxjs';
import { delay, switchMap, filter, take, tap } from 'rxjs/operators';
import { Character } from 'src/app/models';
import { StarWarsDbService } from 'src/app/services/db.service';

interface GenderComboItem {
  key: string;
  value: string;
}

@Component({
  selector: 'sw-add-new-character',
  templateUrl: './add-new-character.component.html',
  styleUrls: ['./add-new-character.component.scss']
})
export class SwAddNewCharacterComponent implements OnInit, AfterViewChecked {
  form: FormGroup;
  genders: GenderComboItem[];
  species: string[];

  characterId: number;
  characterUpdated: boolean;

  private setFocus$ = new BehaviorSubject<boolean>(false);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: StarWarsDbService,
  ) {
    this.genders = [
      { key: 'male', value: 'Male' },
      { key: 'female', value: 'Female' },
      { key: 'n/a', value: 'n/a' },
    ];
  }

  ngOnInit() {
    zip(
      this.service.getSpecies(),
      this.route.paramMap.pipe(
        switchMap((params: ParamMap) => {
          const id = params.get('id');
          if (id) {
            return this.service.getCharacter(id);
          } else {
            return of({} as Character);
          }
        }),
      )
    ).subscribe(([s, c]) => this._initCallback(s, c));
  }

  ngAfterViewChecked() {
    // Poniższa pokrętna (na pierwszy rzut oka) logika wynika ze specyficznego
    // sposobu działania dyrektywy dopisującej do błędnych inputów 'is-invalid'.
    // Aktualizuje się ona na 'ngDoCheck', na etapie zdarzenia 'submit' jeszcze
    // może nie zdążyć zareagować. Z tego powodu na akcji 'submit' ustawiany
    // jest event, który cyklicznie sprawdzam poniżej.
    this.setFocus$.asObservable().pipe(
      take(1),
      filter(x => x === true),
      tap(() => this.setFocus$.next(false))
    ).subscribe(() => (document.querySelector('.is-invalid') as HTMLInputElement).focus());
  }

  submit() {
    if (this.form.invalid) {
      Object.keys(this.form.controls).forEach(control => {
        this.form.get(control).markAsTouched();
      });

      this.setFocus$.next(true);
    } else {
      if (this.characterId) {
        const requestData = this.form.value as Character;
        requestData.id = this.characterId;

        this.service.putCharacter(requestData).subscribe(() => {
          this.characterUpdated = true;
        });
      } else {
        this.service.postCharacter(this.form.value).subscribe(() => {
          this.router.navigate([`/character-list`]);
        });
      }
    }
  }

  private _initCallback(s: string[], c: Character) {
    this.species = s;
    this.characterId = c.id || null;
    this.form = new FormGroup({
      name: new FormControl(c.name || '', Validators.required),
      species: new FormControl(c.species || '', Validators.required),
      gender: new FormControl(c.gender || '', Validators.required),
      homeworld: new FormControl(c.homeworld || ''),
    });
  }
}
