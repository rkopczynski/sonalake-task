import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Character, Gender } from 'src/app/models';
import { StarWarsDbService } from 'src/app/services/db.service';

import { SwAddNewCharacterComponent } from './add-new-character.component';
import { InputInvalidDirective } from 'src/app/common/directives/input-invalid.directive';
import { ValidationMessagesComponent } from 'src/app/common/components/validation-messages/validation-messages.component';

class MockService {
  public character: Character = {
    id: 1,
    gender: 'x' as Gender,
    homeworld: 'x',
    name: 'x',
    species: 'x',
  };

  getSpecies() { return of([]); }
  getCharacter() { return of(this.character); }
  putCharacter() { }
  postCharacter() { }
}

class MockRoute {
  public id = '1';
  public _route = new BehaviorSubject<ParamMap>(<ParamMap>{
    get: (x: any) => {
      return this.id;
    }
  });
  public paramMap = this._route.asObservable();
}

describe('AddNewCharacterComponent', () => {
  let component: SwAddNewCharacterComponent;
  let fixture: ComponentFixture<SwAddNewCharacterComponent>;
  let router: Router;
  let route: MockRoute;
  let service: MockService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: StarWarsDbService, useClass: MockService },
        { provide: ActivatedRoute, useClass: MockRoute },
      ],
      declarations: [
        ValidationMessagesComponent,
        InputInvalidDirective,
        SwAddNewCharacterComponent,
      ],
    }).compileComponents();

    router = TestBed.get(Router);
    route = TestBed.get(ActivatedRoute);
    service = TestBed.get(StarWarsDbService);

    fixture = TestBed.createComponent(SwAddNewCharacterComponent);
    component = fixture.componentInstance;
  });

  it('should create with filled character data', fakeAsync(() => {
    const result: Character = {
      id: 123, gender: Gender.FEMALE, name: 'You', species: 'Sneaky bastard'
    };
    spyOn(service, 'getCharacter').and.returnValue(of(result));
    component.ngOnInit();
    component.ngAfterViewChecked();

    tick();

    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.characterId).toBe(result.id);
    Object.keys(result).filter(key => key !== 'id')
      .forEach(key => expect(component.form.get(key).value).toBe(result[key]));
  }));

  it('should create with empty character data', fakeAsync(() => {
    route.id = null;
    component.ngOnInit();

    tick();

    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.characterId).toBe(null);
    Object.keys(service.character).filter(key => key !== 'id')
      .forEach(key => expect(component.form.get(key).value).toBe(''));
  }));

  it(`shouldn't submit form when any data is invalid`, fakeAsync(() => {
    route.id = null;
    component.ngOnInit();

    tick();

    fixture.detectChanges();
    component.submit();
    fixture.detectChanges();

    tick();

    component.ngAfterViewChecked();

    tick();

    Object.keys(component.form.controls).forEach(key => {
      expect(component.form.get(key).touched).toBeTruthy();
    });
  }));

  it(`should submit form when data is correct (PUT)`, fakeAsync(() => {
    const result: Character = {
      id: 123, gender: Gender.FEMALE, name: 'You', species: 'Sneaky bastard'
    };
    spyOn(service, 'getCharacter').and.returnValue(of(result));
    spyOn(service, 'putCharacter').and.returnValue(of(null));
    component.ngOnInit();

    tick();

    fixture.detectChanges();
    component.submit();
    fixture.detectChanges();

    tick();

    component.ngAfterViewChecked();

    tick();

    expect(component.characterId).toBe(result.id);
    expect(component.characterUpdated).toBeTruthy();
  }));

  it(`should submit form when data is correct (PUT)`, fakeAsync(() => {
    const result: Character = {
      id: 123, gender: Gender.FEMALE, name: 'You', species: 'Sneaky bastard'
    };
    spyOn(service, 'getCharacter').and.returnValue(of(result));
    spyOn(service, 'postCharacter').and.returnValue(of(null));
    const spy = spyOn(router, 'navigate');
    component.ngOnInit();

    tick();

    fixture.detectChanges();
    component.characterId = null;
    component.submit();
    fixture.detectChanges();

    tick();

    component.ngAfterViewChecked();

    tick();

    expect(spy.calls.count()).toBe(1);
  }));
});
