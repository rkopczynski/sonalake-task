import { Component, ElementRef, OnDestroy, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { fromEvent, ReplaySubject } from 'rxjs';
import { takeUntil, throttleTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'sw-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SwSearchComponent implements OnInit, OnDestroy {
  private readonly debounceTime = 200;

  @ViewChild('searchInput') search: ElementRef;
  @Output() searchBarEvent: EventEmitter<string>;

  private destroy$: ReplaySubject<void>;
  private input: HTMLInputElement;

  constructor() {
    this.destroy$ = new ReplaySubject(1);
    this.searchBarEvent = new EventEmitter<string>();
  }

  ngOnInit() {
    this.input = this.search.nativeElement;

    fromEvent(this.search.nativeElement, 'keyup').pipe(
      takeUntil(this.destroy$),
      throttleTime(this.debounceTime),
      map(() => this._checkEmptyString()),
      distinctUntilChanged(),
    ).subscribe(v => this.searchBarEvent.next(v));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public clearInput() {
    this.input.value = null;
    this.searchBarEvent.next(null);
  }

  private _checkEmptyString() {
    if (this.input.value === '') {
      this.input.value = null;
      return null;
    } else {
      return this.input.value;
    }
  }
}
