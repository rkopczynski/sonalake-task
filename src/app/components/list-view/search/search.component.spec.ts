import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwSearchComponent } from './search.component';

describe('SwSearchComponent', () => {
  let component: SwSearchComponent;
  let fixture: ComponentFixture<SwSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SwSearchComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwSearchComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should clear input`, () => {
    component.searchBarEvent.subscribe(event => {
      expect(event).toBeNull();
    });

    component.clearInput();
  });
});
