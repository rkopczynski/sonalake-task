import { Component, OnInit } from '@angular/core';

import { PagedCharacters, Character } from '../../models';
import { StarWarsDbService } from '../../services/db.service';

@Component({
  selector: 'sw-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {
  pagedCharacters: PagedCharacters;
  searchMode: boolean;

  constructor(private service: StarWarsDbService) {
    this.searchMode = false;
  }

  ngOnInit() {
    this._loadCharacters();
  }

  onChangePage(link: string) {
    this.service.getPagedCharacters(link)
      .subscribe(res => this.pagedCharacters = res);
  }

  removeCharacter(char: Character) {
    this.service.removeCharacter(char.id).subscribe(() => this._loadCharacters());
  }

  onSearchChange(text: string) {
    if (text === null) {
      this._loadCharacters();
    } else {
      this.service.getSearchedCharacters(text).subscribe(result => {
        this.searchMode = true;
        this.pagedCharacters = <PagedCharacters>{
          characters: result
        };
      });
    }
  }

  private _loadCharacters() {
    this.service.getInitCharacters().subscribe(res => {
      this.pagedCharacters = res;
      this.searchMode = false;
    });
  }
}
