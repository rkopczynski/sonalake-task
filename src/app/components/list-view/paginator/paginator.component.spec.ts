import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwPaginatorComponent } from './paginator.component';

describe('SwPaginatorComponent', () => {
  let component: SwPaginatorComponent;
  let fixture: ComponentFixture<SwPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SwPaginatorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwPaginatorComponent);
    component = fixture.componentInstance;
    component.chars = {
      characters: [],
      current: `http://localhost:3000/characters?_page=3&_limit=4`,
      first: `http://localhost:3000/characters?_page=1&_limit=4`,
      last: `http://localhost:3000/characters?_page=5&_limit=4`,
      next: `http://localhost:3000/characters?_page=4&_limit=4`,
      prev: `http://localhost:3000/characters?_page=2&_limit=4`,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
