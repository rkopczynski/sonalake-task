import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { PagedCharacters } from '../../../models';

@Component({
  selector: 'sw-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class SwPaginatorComponent implements OnInit {
  @Input() chars: PagedCharacters;

  @Output() changePageEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  changePage(link: string) {
    this.changePageEvent.next(link);
  }

  isCurrent(link: string) {
    return link === this.chars.current;
  }

  getPageNum(link: string) {
    return /_page=(\d*)/.exec(link)[1];
  }
}
