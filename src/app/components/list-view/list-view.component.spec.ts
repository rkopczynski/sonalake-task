import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { PagedCharacters, Character } from 'src/app/models';
import { StarWarsDbService } from 'src/app/services/db.service';

import { ListViewComponent } from './list-view.component';

class MockService {
  getInitCharacters() {
    return of(<PagedCharacters>{
      characters: [],
    });
  }
  getSearchedCharacters() { }
  removeCharacter() { }
  getPagedCharacters() { }
}

describe('ListViewComponent', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;
  let service: StarWarsDbService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListViewComponent],
      providers: [
        { provide: StarWarsDbService, useClass: MockService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    service = TestBed.get(StarWarsDbService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it(`should load characters on init`, fakeAsync(() => {
    const result = <PagedCharacters> {
      characters: [],
    };
    const spy = spyOn(service, 'getInitCharacters').and.returnValue(of(result));

    component.ngOnInit();
    tick();

    expect(spy.calls.count()).toBe(1);
    expect(component.pagedCharacters).toEqual(result);
    expect(component.searchMode).toBeFalsy();
  }));

  it(`should react on 'searchChange' event`, () => {
    const spy = spyOn(service, 'getInitCharacters').and.returnValue(of(null));
    const result = {};
    spyOn(service, 'getSearchedCharacters').and.returnValue(of(result));

    component.onSearchChange(null);
    expect(spy.calls.count()).toBe(1, `For 'null' search call default backend`);
    expect(component.searchMode).toBeFalsy();

    component.onSearchChange('test');
    expect(spy.calls.count()).toBe(1, `For normal search call 'fullsearch' endpoint`);
    expect(component.pagedCharacters).toEqual(<PagedCharacters>{ characters: {} });
    expect(component.searchMode).toBeTruthy();
  });

  it(`should remove character`, () => {
    const remove = spyOn(service, 'removeCharacter').and.returnValue(of(null));
    const init = spyOn(service, 'getInitCharacters').and.returnValue(of(null));

    component.removeCharacter(<Character>{ id: 123 });
    expect(remove.calls.count()).toBe(1);
    expect(init.calls.count()).toBe(1);
  });

  it(`should get paged characters`, () => {
    const spy = spyOn(service, 'getPagedCharacters').and.returnValue(of(null));

    component.onChangePage('link');
    expect(spy.calls.count()).toBe(1);
  });
});
