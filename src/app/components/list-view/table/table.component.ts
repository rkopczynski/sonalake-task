import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Character } from '../../../models';

@Component({
  selector: 'sw-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class SwTableComponent {
  @Input() characters: Character[];
  @Output() removeCharacter = new EventEmitter<number>();

  constructor() { }

  remove(id: number) {
    this.removeCharacter.next(id);
  }
}
