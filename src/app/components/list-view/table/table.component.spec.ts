import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { SwTableComponent } from './table.component';

describe('SwTableComponent', () => {
  let component: SwTableComponent;
  let fixture: ComponentFixture<SwTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [SwTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send event on remove', () => {
    component.removeCharacter.subscribe(event => expect(event).toBe(123));
    component.remove(123);
  });
});
