import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppConfigModule } from './app.config.module';
import { SwAddNewCharacterComponent } from './components/add-new-character/add-new-character.component';
import { ListViewComponent } from './components/list-view/list-view.component';
import { SwPaginatorComponent } from './components/list-view/paginator/paginator.component';
import { SwSearchComponent } from './components/list-view/search/search.component';
import { SwTableComponent } from './components/list-view/table/table.component';
import { ValidationMessagesComponent } from './common/components/validation-messages/validation-messages.component';
import { InputInvalidDirective } from './common/directives/input-invalid.directive';

@NgModule({
  declarations: [
    AppComponent,

    ListViewComponent,
    SwTableComponent,
    SwPaginatorComponent,
    SwSearchComponent,
    SwAddNewCharacterComponent,
    ValidationMessagesComponent,
    InputInvalidDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    AppConfigModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
