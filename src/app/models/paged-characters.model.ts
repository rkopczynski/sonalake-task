import { Character } from './character.model';

export class PagedCharacters {
  /** List of characters on current page */
  characters: Character[];

  /** Link to current page */
  current: string;

  /** Link to characters on previous page */
  prev: string;

  /** Link to characters on next page */
  next: string;

  /** Link to characters on last page */
  last: string;

  /** Link to characters on first page */
  first: string;
}
