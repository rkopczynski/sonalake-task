export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
  OTHER = 'N/A',
}
