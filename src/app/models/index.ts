export { Gender } from './gender.model';
export { Character } from './character.model';
export { PagedCharacters } from './paged-characters.model';