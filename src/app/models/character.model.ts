import { Gender } from './gender.model';

export class Character {
  id: number;
  name: string;
  species: string;
  gender: Gender;
  homeworld?: string;
}
