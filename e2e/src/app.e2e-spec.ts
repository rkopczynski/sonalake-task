import { browser, by } from 'protractor';

import { Character, Gender } from '../../src/app/models';
import { AddCharacterPage } from './add-char.po';
import { CharacterListPage } from './app.po';
import { CharacterRecord } from './models/character-record.model';

describe('sonalake-task-angular App', () => {
  const mainPage = new CharacterListPage();
  const addNewCharPage = new AddCharacterPage();
  const character = <Character>{
    name: 'Grzegorz Brzęczyszczykiewicz',
    species: 'Human',
    gender: Gender.MALE,
    homeworld: '',
  };
  let row: CharacterRecord;

  it(`should display the title`, () => {
    mainPage.navigateTo();
    expect(mainPage.getTitleText()).toEqual('List View');
  });

  it(`should redirect to character creation`, async () => {
    await mainPage.getAddNewButton().click();

    await browser.waitForAngular();
    expect(await addNewCharPage.onAddCharacterPage()).toBeTruthy();
  });

  it(`shouldn't submit unfilled form & should jump to focused 'name' input`, async () => {
    const inputs = addNewCharPage.getFormControls();

    await addNewCharPage.getSubmitButton().click();

    const id = await browser.driver.switchTo().activeElement().getId();
    expect(await inputs.name.getId()).toEqual(id);
  });

  it(`should submit form without 'homeworld`, async () => {
    const inputs = addNewCharPage.getFormControls();

    await inputs.name.sendKeys(character.name);
    await inputs.species.element(by.cssContainingText('option', character.species)).click();
    await inputs.radioMale.click();

    await addNewCharPage.getSubmitButton().click();
    await browser.waitForAngular();
    expect(await mainPage.onThisPage()).toBeTruthy();
  });

  it(`should find inserted character on last grid's 'page'`, async () => {
    await mainPage.goToLastGridPage();
    await browser.waitForAngular();

    row = await mainPage.getLastCharacterRecord();
    expect(row.character.name).toBe(character.name);
    expect(row.character.gender).toBe(character.gender);
    expect(row.character.species).toBe(character.species);
    expect(row.character.homeworld).toBe(character.homeworld);
  });

  it(`should edit character`, async () => {
    await row.editBtn.click();
    await browser.waitForAngular();
    expect(await addNewCharPage.onEditCharacterPage()).toBeTruthy();

    character.homeworld = 'Earth';
    await addNewCharPage.getFormControls().homeWorld.sendKeys(character.homeworld);
    await addNewCharPage.getSubmitButton().click();
    await browser.waitForAngular();

    expect(await addNewCharPage.alertVisible()).toBeTruthy();

    await mainPage.navigateTo();
    await browser.waitForAngular();

    await mainPage.goToLastGridPage();
    await browser.waitForAngular();

    row = await mainPage.getLastCharacterRecord();
    expect(row.character.homeworld)
      .toBe(character.homeworld);
  });

  it(`should find new character using search bar`, async () => {
    const sb = mainPage.getSearchBar();
    await sb.input.sendKeys(character.name);
    await browser.waitForAngular();

    const toCompare = await mainPage.getLastCharacterRecord();
    expect(row.character).toEqual(toCompare.character);
  });

  it(`should show info-alert when search didn't find anything`, async () => {
    const sb = mainPage.getSearchBar();
    await sb.input.sendKeys(`TEST${character.name}TEST`);
    await browser.waitForAngular();

    expect(await mainPage.alertVisible()).toBeTruthy();
    await sb.button.click();
    await browser.waitForAngular();
  });

  it(`should remove character`, async () => {
    await mainPage.goToLastGridPage();
    await browser.waitForAngular();
    row = await mainPage.getLastCharacterRecord();

    await row.removeBtn.click();
    await browser.waitForAngular();

    await mainPage.goToLastGridPage();
    await browser.waitForAngular();

    const someRow = await mainPage.getLastCharacterRecord();
    expect(someRow.character.id !== row.character.id).toBeTruthy();
  });
});
