import { browser, by, element } from 'protractor';

import { Character, Gender } from '../../src/app/models';
import { CharacterRecord } from './models/character-record.model';
import { SearchBar } from './models/search-bar.model';

export class CharacterListPage {
  navigateTo() {
    return browser.get('/');
  }

  async onThisPage(): Promise<boolean> {
    return (await browser.getCurrentUrl()).includes('character-list');
  }

  getTitleText() {
    return element(by.css('h1')).getText();
  }

  getAddNewButton() {
    return element(by.css('.btn.btn-primary.mb-3'));
  }

  goToLastGridPage() {
    return element.all(by.css('a.page-link')).last().click();
  }

  getSearchBar(): SearchBar {
    return {
      input: element(by.css('.input-group.mb-3 > input')),
      button: element(by.css('.input-group.mb-3 button')),
    };
  }

  async alertVisible(): Promise<boolean> {
    return await element(by.css('.alert.alert-primary')).isPresent();
  }

  async getLastCharacterRecord(): Promise<CharacterRecord> {
    const id = element(by.css('tbody > tr:last-child > th'));
    const char = element.all(by.css('tbody > tr:last-child > td'));
    const buttons = await element.all(by.css('tbody > tr:last-child button'));

    const character = new Character();
    character.id = Number(await id.getText());
    character.name = await char.get(0).getText();
    character.species = await char.get(1).getText();
    character.gender = await char.get(2).getText() as Gender;
    character.homeworld = (await char.get(3).getText()) || '';

    const record = new CharacterRecord();
    record.character = character;
    record.editBtn = buttons[0];
    record.removeBtn = buttons[1];

    return record;
  }
}
