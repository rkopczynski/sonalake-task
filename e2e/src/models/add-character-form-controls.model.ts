import { ElementFinder } from 'protractor';

export class AddCharacterFormControls {
    name: ElementFinder;
    species: ElementFinder;
    radioMale: ElementFinder;
    radioFemale: ElementFinder;
    radioNA: ElementFinder;
    homeWorld: ElementFinder;
}
