import { ElementFinder } from 'protractor';

import { Character } from '../../../src/app/models';

export class CharacterRecord {
    character: Character;
    editBtn: ElementFinder;
    removeBtn: ElementFinder;
}
