import { ElementFinder } from 'protractor';

export class SearchBar {
    input: ElementFinder;
    button: ElementFinder;
}
