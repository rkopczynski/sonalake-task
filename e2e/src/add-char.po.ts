import { browser, by, element, ElementFinder } from 'protractor';

import { AddCharacterFormControls } from './models/add-character-form-controls.model';

export class AddCharacterPage {
  navigateTo() {
    return browser.get('/add-new-character');
  }

  async onAddCharacterPage(): Promise<boolean> {
    return (await browser.getCurrentUrl()).includes('add-new-character');
  }

  async onEditCharacterPage(): Promise<boolean> {
    return (await browser.getCurrentUrl()).includes('edit-character');
  }

  async alertVisible(): Promise<boolean> {
    return await element(by.css('.alert.alert-success')).isPresent();
  }

  getTitleText() {
    return element(by.css('h1')).getText();
  }

  getSubmitButton(): ElementFinder {
    return element(by.css('.btn.btn-primary'));
  }

  getFormControls(): AddCharacterFormControls {
    return {
      name: element(by.css('input#nameInput')),
      species: element(by.css('select#speciesSelect')),
      radioMale: element(by.css('input#radiomale')),
      radioFemale: element(by.css('input#radiofemale')),
      radioNA: element(by.css('input#radion/q')),
      homeWorld: element(by.css('input#homeworldInput')),
    };
  }
}
